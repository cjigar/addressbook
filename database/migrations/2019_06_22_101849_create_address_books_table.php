<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressBooksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('address_books', function(Blueprint $table)
		{
            $table->increments('id');
            $table->string('title', 50);
            $table->string('name',150);
            $table->string('contact_no',50);
            $table->string('address1',255);
            $table->string('address2',255);
            $table->string('address3',255);
            $table->string('pincode',20);
            $table->string('city',50);
            $table->string('state',50);
            $table->string('country',50);
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('address_books');
	}

}
