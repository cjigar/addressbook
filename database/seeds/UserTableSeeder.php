<?php
use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder {

    public function run() {

        DB::table('users')->delete();

        User::create([
           'name' => 'Jigar Chaudhary',
           'email' => 'admin@gmail.com',
           'password' => Hash::make('admin'),
           'remember_token' => str_random(10),
        ]);
    }
}