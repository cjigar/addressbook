<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class AddressBook extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'address_books';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title','name','contact_no','address1','address2','address3','pincode','city','state','country'];

}
