<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class AddressBooksRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			//
            'title' => 'required|max:50',
            'name' => 'required|max:150',
            'contact_no' => 'required|max:50',
            'address1' => 'max:255',
            'address2' => 'max:255',
            'address3' => 'max:255',
            'pincode' => 'numeric|max:20',
            'city' => 'required|max:150',
            'state' => 'max:150',
            'country' => 'required',
		];
	}

}
