<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\AddressBooksRequest;
use App\AddressBook;
use Illuminate\Support\Facades\Session;

class AddressBooksController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
        return view('addressbooks.create');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
        return view('addressbooks.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(AddressBooksRequest $request) {
		//
        $post_data = $request->except('_token');
        if(!empty(AddressBook::create($post_data)->id)) {
            //Session::flash('success', 'Addressbooks has beed stored successfully!');
        } else {
            //Session::flash('fail','Opps try again !');
            redirect()->route('addressbooks.create')
                ->withErrors([
                    'title' => 'Opps try again !',
                ]);

        }
        return redirect()->route('addressbooks.create');

    }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
