# Address Book in Laravel 5.0 using MySQL
For the test you need to create an Address Book in Laravel 5.0 using MySQL as the DB.
## Part 1

#### Frame 1
This frame will contain a login screen, where a user will be able to login using his username &
password.

#### Frame 2
This frame will contain two options Manage Profile and Manage Address Book

#### Frame3
This frame will contain the Manage Address Book functionality.
Fields for each Address Book entry:

a. Address Book Title ( Home, Work, etc.)
b. Contact Person Name
c. Contact Person Number
d. Address Line 1
e. Address Line 2
f. Address Line 3
g. Pincode
h. City
i. State
j. Country

A user should be able to Add, Edit, Delete Addresses. There can be 2 default addresses, a ‘default
from’ address and a ‘default to’ address.

## Part 2
a. Create a RESTful API to manage the address book made in Part 1
b. Create a sample code to implement your API

## Deliverables:
● Source code of the above solution (preferably as a git repository on bitbucket).
● Attach any instructions you feel necessary to run the solution.
● Include test data for the project

